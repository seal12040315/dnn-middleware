#!/bin/python
# Copyright      2019   Moez Ajili
#                2019   MyVoice AI
# Apache 2.0
import logging
import librosa
import numpy as np
import os,sys,math
import torch
import torch.nn as nn
import torch.nn.functional as F
import pyaudio


def normalize(yt):
    yt_max = np.max(yt)
    yt_min = np.min(yt)
    a = 1.0 / (yt_max - yt_min)
    b = -(yt_max + yt_min) / (2 * (yt_max - yt_min))
    yt = yt * a + b
    return yt

def load_model(use_cuda, pretrain_pth,embedding_size, n_classes):
    model=xvectorLight()
    if use_cuda:
        model.cuda()
    #print('=> loading checkpoint')
    # original saved file with DataParallel
    checkpoint = torch.load(str(pretrain_pth),map_location=torch.device('cpu') )
    # create new OrderedDict that does not contain `module.`
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    return model

def extract_feature(input_file, feature='fbank', dim=40, cmvn=False, delta=False, delta_delta=False,
                    window_size=25, stride=10, save_feature=None,sample_rate=16000):
    y, sr = librosa.load(input_file, sr=sample_rate)
    yt, _ = librosa.effects.trim(y, top_db=20)
    yt = normalize(yt)
    ws = int(sr * 0.001 * window_size)
    st = int(sr * 0.001 * stride)
    if feature == 'fbank':  # log-scaled
        feat = librosa.feature.melspectrogram(y=yt, sr=sr, n_mels=dim, n_fft=ws, hop_length=st)
        feat = np.log(feat + 1e-6)
    elif feature == 'mfcc':
        feat = librosa.feature.mfcc(y=yt, sr=sr, n_mfcc=dim, n_mels=26, n_fft=ws, hop_length=st)
        feat[0] = librosa.feature.rmse(yt, hop_length=st, frame_length=ws) #rmse

    else:
        raise ValueError('Unsupported Acoustic Feature: ' + feature)

    feat = [feat]
    if delta:
        feat.append(librosa.feature.delta(feat[0]))

    if delta_delta:
        feat.append(librosa.feature.delta(feat[0], order=2))
    feat = np.concatenate(feat, axis=0)
    if cmvn:
        feat = (feat - feat.mean(axis=1)[:, np.newaxis]) / (feat.std(axis=1) + 1e-16)[:, np.newaxis]
    return feat

def save_feature(path,input_file_vector,name):
    if os.path.isdir(path)==False:
        os.makedirs(path) 
    np.savez(path+name,input_file_vector)

def build_dict(wav_scp):
    wavId_path = {}
    with open(wav_scp, 'rt') as dataFile:
        for line in dataFile:
            wavId, path = line.split()
            wavId_path[wavId] = path
    return wavId_path

class TDNN(nn.Module):
    
    def __init__(
                    self, 
                    input_dim=80, 
                    output_dim=512,
                    context_size=5,
                    stride=1,
                    dilation=1,
                    batch_norm=True,
                    dropout_p=0.0
                ):
        super(TDNN, self).__init__()
        self.context_size = context_size
        self.stride = stride
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.dilation = dilation
        self.dropout_p = dropout_p
        self.batch_norm = batch_norm
      
        self.kernel = nn.Linear(input_dim*context_size, output_dim)
        self.nonlinearity = nn.ReLU()
        if self.batch_norm:
            self.bn = nn.BatchNorm1d(output_dim)
        if self.dropout_p:
            self.drop = nn.Dropout(p=self.dropout_p)
        
    def forward(self, x):
        _, _, d = x.shape
        assert (d == self.input_dim), 'Input dimension was wrong. Expected ({}), got ({})'.format(self.input_dim, d)
        x = x.unsqueeze(1)
        x = F.unfold(
                        x, 
                        (self.context_size, self.input_dim), 
                        stride=(1,self.input_dim), 
                        dilation=(self.dilation,1)
                    )
        x = x.transpose(1,2)
        x = self.kernel(x)
        x = self.nonlinearity(x)
        if self.dropout_p:
            x = self.drop(x)
        if self.batch_norm:
            x = x.transpose(1,2)
            x = self.bn(x)
            x = x.transpose(1,2)
        return x

class statPool(nn.Module):
    def __init__(self):
        super(statPool, self).__init__()
    def forward(self, x):
        _ ,_ ,d = x.shape
        mu = torch.mean(x,dim=1)
        std = torch.var(x,dim=1) 
        mustd = torch.cat([mu, std], dim=1)
        return mustd

class xvectorLight(nn.Module):
    def __init__(self, num_classes=1211):
        super(xvectorLight, self).__init__()
        self.batch_norm = True
        self.f1 = TDNN(input_dim=80, output_dim=256, context_size=5, dilation=1)
        self.f2 = TDNN(input_dim=256, output_dim=256, context_size=3, dilation=2)
        self.statPool= statPool()
        self.relu = nn.ReLU()
        self.fc1 = nn.Linear(256*2, 256)
        self.bn1=nn.BatchNorm1d(256*2)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(1211)
        self.fc2 = nn.Linear(256,num_classes)
        self.softmax = nn.LogSoftmax()
    def forward(self, inputs):
        f1_out = F.relu(self.f1(inputs))
        f2_out = F.relu(self.f2(f1_out))
        f_poul = self.statPool(f2_out)
        if self.batch_norm:
            f_poul = self.bn1(f_poul)
        spk_embedding = self.fc1(f_poul)
        return spk_embedding, spk_embedding
        '''f_poul = F.relu(spk_embedding)
        if self.batch_norm:
            f_poul = self.bn2(f_poul)
        fc2_out = F.relu(self.fc2(f_poul))
        if self.batch_norm:
            fc2_out = self.bn3(fc2_out)
        f_sof = F.log_softmax(fc2_out,dim=1)
        return spk_embedding, fc2_out'''


class ToTensorTestInput(object):
    """Convert ndarrays in sample to Tensors."""
    def __call__(self, np_feature):
        """
        Args:
            feature (numpy.ndarray): feature to be converted to tensor.
        Returns:
            Tensor: Converted feature.
        """
        if isinstance(np_feature, np.ndarray):
            # handle numpy array
            np_feature = np.expand_dims(np_feature, axis=0)
            np_feature = np.expand_dims(np_feature, axis=1)
            assert np_feature.ndim == 4, 'Data is not a 4D tensor. size:%s' %(np.shape(np_feature),)
            #print(np_feature.shape)
            #np_feature = np.transpose(np_feature[:,0,:,:],(0, 2, 1))#.contiguous()
            np_feature = np_feature[:,0,:,:]
            #print(np_feature.shape)
            ten_feature = torch.from_numpy(np_feature).float() # output type => torch.FloatTensor, fast
            # input size : (1, 1, n_win=200, dim=40)
            # output size : (1, 1, dim=40, n_win=200)
            return ten_feature


def l2_norm(input, alpha):
    input_size = input.size()  # size:(n_frames, dim)
    buffer = torch.pow(input, 2)  # 2 denotes a squared operation. size:(n_frames, dim)
    normp = torch.sum(buffer, 1).add_(1e-10)  # size:(n_frames)
    norm = torch.sqrt(normp)  # size:(n_frames)
    _output = torch.div(input, norm.view(-1, 1).expand_as(input))
    output = _output.view(input_size)
    # Multiply by alpha = 10 as suggested in https://arxiv.org/pdf/1703.09507.pdf
    output = output * alpha
    return output

def get_embeddings(profileID, feat, model, test_frames):
    tot_segments = math.ceil(len(feat)/test_frames)
    model.eval()
    path_vp="./vp/"
    activation = 0
    with torch.no_grad():
        for i in range(int(tot_segments)):
            temp_feat = feat[i*test_frames:i*test_frames+test_frames]
            TT = ToTensorTestInput()
            temp_input = TT(temp_feat)
            spk_embedding, _ = model(temp_input)
            activation += torch.sum(spk_embedding, dim=0, keepdim=True)
        spk_embedding = l2_norm(activation, 1)
        #print(spk_embedding.shape)
    return spk_embedding

def get_embeddings_stream(profileID, feat, model):
    model.eval()
    path_vp="./vp/"
    activation = 0
    with torch.no_grad():
        TT = ToTensorTestInput()
        temp_input = TT(feat)

        spk_embedding, _ = model(temp_input)
        #activation += torch.sum(spk_embedding, dim=0, keepdim=True)
        spk_embedding = l2_norm(spk_embedding, 1)
        #print(spk_embedding.shape)
    return spk_embedding

def main():
    use_cuda = False
    log_dir = 'model_saved'
    embedding_size = 128
    n_classes = 1211
    sample_rate=16000
    test_frames = 300
    PATH_ENGINE=os.getcwd()
    pretrain_pth=PATH_ENGINE +'/px/n.nx'
    model = load_model(use_cuda, pretrain_pth,embedding_size, n_classes)
    mode=str(sys.argv[1])
    listF=str(sys.argv[2])
    profileID=str(sys.argv[3])
    tmp = PATH_ENGINE + "/tmp/"
    if os.path.isdir(tmp)==False:
        os.makedirs(tmp)
    scp_output=tmp+'feat.scp'
    f = open('./list/'+listF , "r")
    lines=f.readlines()
    f.close()
    with open(scp_output, 'wt' ) as f:
        sample_rate=16000
        for key in lines:
            key=key.rstrip()
            input_file="./wav/"+key+".wav"
            input_file_vector=extract_feature(input_file, feature='fbank', dim=80, cmvn=True, delta=False, delta_delta=False, window_size=25, stride=10, save_feature=None,sample_rate=sample_rate)
            input_file_vector=np.swapaxes(input_file_vector, 0, 1).astype('float32')
            save_feature(tmp,input_file_vector,key)
            message=key+" "+tmp+key+".npz\n"
            f.write(message)
    get_embeddings(profileID, input_file_vector, model, test_frames)

def enrol():
    use_cuda = False
    embedding_size = 256
    n_classes = 1211
    sample_rate=16000
    test_frames = 300
    PATH_ENGINE=os.getcwd()
    pretrain_pth=PATH_ENGINE +'/sys/px/n.nx'
    path_vp = PATH_ENGINE +"/sys/vp/"
    model = load_model(use_cuda, pretrain_pth,embedding_size, n_classes)
    mode=str(sys.argv[1])
    listF=str(sys.argv[2])
    profileID=str(sys.argv[3])
    tmp = PATH_ENGINE + "/sys/tmp/"
    if os.path.isdir(tmp)==False:
        os.makedirs(tmp)
    scp_output=tmp+'feat.scp'
    f = open('./sys/list/'+listF , "r")
    lines=f.readlines()
    f.close()
    with open(scp_output, 'wt' ) as f:
        sample_rate=16000
        for key in lines:
            key=key.rstrip()
            input_file="./sys/wav/"+key+".wav"
            input_file_vector=extract_feature(input_file, feature='fbank', dim=80, cmvn=True, delta=False, delta_delta=False, window_size=25, stride=10, save_feature=None,sample_rate=sample_rate)
            input_file_vector=np.swapaxes(input_file_vector, 0, 1).astype('float32')
            save_feature(tmp,input_file_vector,key)
            message=key+" "+tmp+key+".npz\n"
            f.write(message)
    #print(input_file_vector, input_file_vector.shape)
    spk_embedding = get_embeddings(profileID, input_file_vector, model, test_frames)
    #print(spk_embedding)
    save_feature(path_vp, spk_embedding, profileID)
    print('enrolment')

def verify():
    use_cuda = False
    log_dir = 'model_saved'
    embedding_size = 128
    n_classes = 1211
    sample_rate=16000
    test_frames = 300
    PATH_ENGINE=os.getcwd()
    pretrain_pth=PATH_ENGINE +'/sys/px/n.nx'
    model = load_model(use_cuda, pretrain_pth,embedding_size, n_classes)
    mode=str(sys.argv[1])
    listF=str(sys.argv[3])
    profileID=str(sys.argv[2])
    pathv1=PATH_ENGINE+"/sys/vp/"+profileID+".npz"
    en_embgs = torch.from_numpy(np.load(pathv1)["arr_0"])  
    sample_rate=16000
    input_file="./sys/wav/"+ listF +".wav"
    input_file_vector=extract_feature(input_file, feature='fbank', dim=80, cmvn=True, delta=False, delta_delta=False, window_size=25, stride=10, save_feature=None,sample_rate=sample_rate)
    input_file_vector=np.swapaxes(input_file_vector, 0, 1).astype('float32')
    spk_embedding = get_embeddings(profileID, input_file_vector, model, test_frames)
    score=np.sum(np.dot(en_embgs.cpu().numpy(),spk_embedding[0].cpu().numpy()))
    score_cal=pow(10,25.184*score-18.056)
    print(format(score_cal, '.2f'))

def verifyStream():
    use_cuda = False
    log_dir = 'model_saved'
    embedding_size = 128
    n_classes = 1211
    sample_rate=16000
    test_frames = 300
    PATH_ENGINE=os.getcwd()
    pretrain_pth=PATH_ENGINE +'/sys/px/n.nx'
    model = load_model(use_cuda, pretrain_pth,embedding_size, n_classes)
    mode=str(sys.argv[1])
    profileID1=str(sys.argv[3])
    profileID=str(sys.argv[2])
    pathv1=PATH_ENGINE+"/sys/vp/"+profileID+".npz"
    pathv2=PATH_ENGINE+"/sys/vp/"+profileID1+".npz"
    en_embgs1 = torch.from_numpy(np.load(pathv1)["arr_0"])  
    en_embgs2 = torch.from_numpy(np.load(pathv2)["arr_0"])
    sample_rate=16000

    chunk = 1024  # Record in chunks of 1024 samples
    sample_format = pyaudio.paInt16  # 16 bits per sample
    channels = 1
    fs = 16000  # Record at 44100 samples per second
    sr=16000
    stride=10 
    test_frames=300
    window_size=25
    seconds = 3
    p = pyaudio.PyAudio()  # Create an interface to PortAudio
    print('Recording')
    stream = p.open(format=sample_format,
                channels=channels,
                rate=fs,
                frames_per_buffer=chunk,
                input=True)
    ws = int(sr * 0.001 * window_size)
    st = int(sr * 0.001 * stride)
    while 1>0:
        frames = []  # Initialize array to store frames
        y=np.array([])
        # Store data in chunks for 3 seconds
        for i in range(0, int(fs / chunk * seconds)):
            data = stream.read(chunk)
            frames.append(data)
            narr=np.frombuffer(data, dtype=np.int16)
            y=np.concatenate((y,narr), axis=0)
        yt, _ = librosa.effects.trim(y, top_db=20)
        yt = normalize(yt)
        feat = librosa.feature.melspectrogram(y=yt, sr=sample_rate, n_mels=80, n_fft=ws, hop_length=st)
        feat = np.log(feat + 1e-6)
        feat = [feat]
        feat = np.concatenate(feat, axis=0)
        feat = (feat - feat.mean(axis=1)[:, np.newaxis]) / (feat.std(axis=1) + 1e-16)[:, np.newaxis]
        input_file_vector=np.swapaxes(feat, 0, 1).astype('float32')
        input_file_vector=input_file_vector[0:300]
        print(input_file_vector.shape)
        spk_embedding = get_embeddings_stream(profileID, input_file_vector, model)
        score1=np.sum(np.dot(en_embgs1.cpu().numpy(),spk_embedding[0].cpu().numpy()))
        score_cal1=pow(10,25.184*score1-18.056)
        score2=np.sum(np.dot(en_embgs2.cpu().numpy(),spk_embedding[0].cpu().numpy()))
        score_cal2=pow(10,25.184*score2-18.056)
        thres=100
        if score_cal1>score_cal2 and score_cal1>thres:
            print(profileID)
        elif score_cal2>score_cal1 and score_cal2>thres:
            print(profileID1)
        else:
            print('unknown speaker', format(score_cal1, '.10f'), format(score_cal2, '.10f'))
        #print(spk_embedding)
    
    # Stop and close the stream 
    stream.stop_stream()
    stream.close()
    # Terminate the PortAudio interface
    p.terminate()

if __name__ == '__main__':
    if (len(sys.argv) < 4):
        print("this script take exactly three arguments, mode{train,dev,eval}, path and wav_scp.",len(sys.argv)-1," are given")
        sys.exit(0)
    else:
        if(sys.argv[1]=="0"):
            # print("enrolement")
            enrol()
        elif (sys.argv[1]=="1"):
            # print("verification")
            verify()
        elif (sys.argv[1]=="2"):
            print("streaming verification")
            verifyStream()
        #main()
        exit(0)

