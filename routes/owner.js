var express = require('express');
var router = express.Router();
var TokenGenerator = require('uuid-token-generator');
var ownerModel = require('../models/owner');
var tokgen = new TokenGenerator(256, TokenGenerator.BASE62);

router.post('/register', async (req, res) => {
    try {
        let owner = await ownerModel.create({
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            domain: req.body.domain,
            token: tokgen.generate()
        });
        console.log('owner----->', owner);
        res.json({token: owner.token});
    } catch (error) {
        console.log('error----->', error);
        res.json({error: error.errmsg});
    }
});

module.exports = router;